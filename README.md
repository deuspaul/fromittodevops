# From IT to DevOps

This Gitlab project contains the associated code for the course titled: "From IT to DevOps".

<br>

# Project Links

### Initial Calcyoulater project:
https://gitlab.com/deuspaul/calcyoulater-init

### Example Calcyoulater project with subtraction feature and gitlab-ci.yml(pipeline) file:
https://gitlab.com/deuspaul/calcyoulater-init

### Final Calcyoulater project:
https://gitlab.com/deuspaul/calcyoulater-complete

<br>

# Associated Code:

### Lab 5:
#### gitlab-ci.yml file:
https://gitlab.com/deuspaul/calcyoulater-lab/-/blob/main/.gitlab-ci.yml
#### server.py file:
https://gitlab.com/deuspaul/calcyoulater-lab/-/blob/main/server.py
#### unit_tests file:
https://gitlab.com/deuspaul/calcyoulater-lab/-/blob/main/unit_tests.py
<br>
<br>
### Lab 6:
#### setup.py file:
https://gitlab.com/deuspaul/fromittodevops/-/blob/main/setup.py
#### MANIFEST.in file:
https://gitlab.com/deuspaul/fromittodevops/-/blob/main/MANIFEST.in
#### 6.6 lab-c gitlab-ci.yml file:
https://gitlab.com/deuspaul/fromittodevops/-/blob/main/6_6lab_c-gitlab-ci.yml
#### Dockerfile file:
https://gitlab.com/deuspaul/fromittodevops/-/blob/main/Dockerfile
#### 6.7 lab-d gitlab-ci.yml file:
https://gitlab.com/deuspaul/fromittodevops/-/blob/main/6_6lab_d-gitlab-ci.yml
#### 6.8 .npmrc file:
https://gitlab.com/deuspaul/fromittodevops/-/blob/main/.npmrc
#### 6.8 package.json file:
https://gitlab.com/deuspaul/fromittodevops/-/blob/main/package.json
#### 6.8 ci_settings.xml file:
https://gitlab.com/deuspaul/fromittodevops/-/blob/main/ci_settings.xml
#### 6.8 pom.xml file:
https://gitlab.com/deuspaul/fromittodevops/-/blob/main/pom.xml
<br>
<br>
### Lab 7:
#### 7.4 lab-b gitlab-ci.yml file:
https://gitlab.com/deuspaul/fromittodevops/-/blob/main/7_4lab_b-gitlab-ci.yml
#### 7.5 perfload.yaml file:
https://gitlab.com/deuspaul/fromittodevops/-/blob/main/perfload.yaml
#### 7.6 lab-c gitlab-ci.yml file:
https://gitlab.com/deuspaul/fromittodevops/-/blob/main/7_5lab_c-gitlab-ci.yml
<br>
<br>
### Lab 9:
#### 9.8 lab-b gitlab-ci.yml file:
https://gitlab.com/deuspaul/fromittodevops/-/blob/main/9_8lab_b-gitlab-ci.yml
<br>
<br>
# Contact

Feel free to reach out to me:

### LinkedIn
https://www.linkedin.com/in/peafl/

### Twitter
https://twitter.com/deusPaul
